## 0.4.0 / 2024-06-08
* Update `sea-query` dependency to 0.30.
* Add new `block` and `block_target` tables, remove `ipblocks`.
* Remove `externallinks`'s `el_index`, `el_index_60` and `el_to` columns.
* Add `pagelinks.pl_target_id` column.
* Remove `revision_comment_temp` table.
* Add `user.user_is_temp` column.
* Add `user_autocreate_serial.uas_year` column.

## 0.3.0 / 2023-01-19
* Move to separate [Git repository](https://gitlab.wikimedia.org/repos/mwbot-rs/mwseaql/).
* Update `sea-query` dependency to new [0.28 release](https://www.sea-ql.org/blog/2022-12-30-whats-new-in-seaquery-0.28.0/).
* Raise MSRV to 1.62.
* Add more `_userindex` tables present on Toolforge.
* Add `actor_` table variants present on Toolforge.

## 0.2.0 / 2022-11-03
* Update `externallinks` schema.
* Update `sea-query` dependency to new [0.27 release](https://www.sea-ql.org/blog/2022-10-31-whats-new-in-seaquery-0.27.0/).

## 0.1.2 / 2022-10-05
* Re-export `sea_query` for convenience of downstream users.

## 0.1.1 / 2022-10-01
* Git repository moved to [Wikimedia GitLab](https://gitlab.wikimedia.org/repos/mwbot-rs/mwbot).
* Issue tracking moved to [Wikimedia Phabricator](https://phabricator.wikimedia.org/project/profile/6182/).

## 0.1.0 / 2022-09-19
* Initial release.
