// This file is autogenerated.
//! Schema definitions for the `mediawiki/extensions/ProofreadPage` repository
use sea_query::IdenStatic;

/// `pr_index` table
///
/// Documentation may be available on [mediawiki.org](https://www.mediawiki.org/wiki/Manual:pr_index_table).
#[non_exhaustive]
#[derive(Copy, Clone, IdenStatic)]
#[iden = "pr_index"]
pub enum PrIndex {
    Table,
    /// `pr_count` column
    #[iden = "pr_count"]
    Count,
    /// `pr_page_id` column
    #[iden = "pr_page_id"]
    PageId,
    /// `pr_q0` column
    #[iden = "pr_q0"]
    Q0,
    /// `pr_q1` column
    #[iden = "pr_q1"]
    Q1,
    /// `pr_q2` column
    #[iden = "pr_q2"]
    Q2,
    /// `pr_q3` column
    #[iden = "pr_q3"]
    Q3,
    /// `pr_q4` column
    #[iden = "pr_q4"]
    Q4,
}
