mwseaql
============
[![crates.io](https://img.shields.io/crates/v/mwseaql.svg)](https://crates.io/crates/mwseaql)
[![docs.rs](https://docs.rs/mwseaql/badge.svg)](https://docs.rs/mwseaql)

The `mwseaql` crate contains [MediaWiki SQL table definitions](https://www.mediawiki.org/wiki/Manual:Database_layout) for use with [SeaQL's SeaQuery builder](https://docs.rs/sea-query/).

See the [full documentation](https://docs.rs/mwseaql/) (docs for [main](https://mwbot-rs.gitlab.io/mwbot/mwseaql/)).

## License
mwseaql is (C) 2022 Kunal Mehta, released under the GPL v3 or any later version,
see COPYING for details.
